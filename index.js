const { calculateLightness, calculateSaturation, calculateHue, calculateRGBfromHSL} = require("./rgbhsl")

const hexStringToRgbArray = (hexString) => {
    const hexStringWithoutHashtag = hexString.replace("#", "")
    const rgbArrayHex = hexStringWithoutHashtag.match(/.{1,2}/g);
    return rgbArrayHex.map(hexString => parseInt(hexString, 16))
}

// regular expression tester: https://regex101.com/
const rgbStringToRgbArray = (rgbString) => {
    const rgbStringWithoutBeginningAndEnd = rgbString.replace("rgb(", "").replace(")", "")
    const rgbArrayOfStrings = rgbStringWithoutBeginningAndEnd.split(",")
    return rgbArrayOfStrings.map(rgbString => parseInt(rgbString, 10))
}

const colorStringToRgbArray = (colorString) => {

    const hexStringRegExp = /^\#[a-fA-F0-9]{6}$/;
    const rgbStringRegExp = /^rgb\(\s*[0-9]{1,3},\s*[0-9]{1,3},\s*[0-9]{1,3}\s*\)$/
    if(hexStringRegExp.test(colorString)) {
        return hexStringToRgbArray(colorString)
    } else if(rgbStringRegExp.test(colorString)) {
        return rgbStringToRgbArray(colorString)
    } else {
        throw "Color string must either be in format '#XXYYZZ' or in format 'rgb(R,G,B)'."
    }
}


const rgbArrayToRgbString = (rgbArray) => {
    if(rgbArray.length !== 3) {
        throw "Rgb must contain 3 numbers."
    }
    return `rgb(${rgbArray[0]}, ${rgbArray[1]}, ${rgbArray[2]})`
}

const changeLightness = (color, lightnessChange) => {
    const rgbArray = colorStringToRgbArray(color)
    const lightness = calculateLightness(...rgbArray)
    const hue = calculateHue(...rgbArray)
    const saturation = calculateSaturation(...rgbArray)
    const modifiedLightness = Math.min(Math.max(lightness + lightnessChange, 0), 100)
    const modifiedRgbArray = calculateRGBfromHSL(hue, saturation, modifiedLightness)
    return rgbArrayToRgbString([modifiedRgbArray.R, modifiedRgbArray.G, modifiedRgbArray.B])
}

module.exports = { changeLightness }
